/**
 * Contract: Split
 * Author: Michael Taylor
 * Description: This contract provides functions that allow the creator
 * of the contract to maintain a collection of coin addresses and their weights.
 * Once established, any eth that is sent to this contract will be automatically 
 * split up by their weights and send to each address.
 * 
 * When paired with exchange services like shapeshift.io and coinbase recurring transactions,
 * This can be used as a means to automatically invest accross a broad spectrum of crypto.     
 */
pragma solidity ^0.4.10;


contract Split {
    address public creator;
    mapping(uint8 => address) public addresses;
    
    // As whole numbers that add up to 100
    mapping(uint8 => uint8) public weights;
    
    // Running count of current weights, used to prevent going past 100.
    uint8 public currentTotalWeight = 0;

    // By keeping an index of how many coins we want are using
    // We can just ignore the indexes we are no longer using
    // and just overwrite indexes that we want to add.
    // This is much cheaper in gas cost, then deleting mapping elements.
    uint8 public numCoins = 0;

    // Equivalent to 0.1 Ether
    // We need a minDeposit, because if we send amounts to small,
    // they might not be accepted as inputs at exchanges.
    uint256 public minDepositInWei = 100000000000000000;

    function Split() public {
        creator = msg.sender;
    }

    function () public payable {
        // add if wei > 0.1 ether
        splitBalance();
    }

    function addSplitAddr(uint8 weight, address addr) public {
        require(msg.sender == creator);
        require((currentTotalWeight + weight) <= 100);
        addresses[numCoins] = addr;
        weights[numCoins] = weight;
        currentTotalWeight += weight;
        numCoins = numCoins + 1;
    }

    function removeLastAddr() public {
        require(numCoins != 0 && msg.sender == creator);
        currentTotalWeight -= weights[numCoins];
        numCoins -= 1;
    }

    function clearAllAddr() public {
        require(msg.sender == creator);
        currentTotalWeight = 0;
        numCoins = 0;
    }

    function changeRequiredMinDeposit(uint256 value) public {
        require(msg.sender == creator);
        minDepositInWei = value;
    }

    // Use this to empty the contract of any residual funds
    // That have not met the minimum threshold needed to initiate the autosplit
    function withdrawlAll() public {
        require(msg.sender == creator);
        address myAddress = this;
        creator.transfer(myAddress.balance);
    }

    function splitBalance() private {
        address myAddress = this;
        uint256 myBalance = myAddress.balance;
        for (uint8 i = 0; i <= numCoins; ++i) {
            uint256 sendAmount = myBalance * (weights[i] / 100);
            if ((myBalance - sendAmount) > 0) {
                addresses[i].transfer(sendAmount);
            }
        }
    }

    // No self destruct method, if your done with the contract just call wdrlBalance()
    // And forget about it.
}